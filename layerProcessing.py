# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# layerProcessing
# Usage: layerProcessing <mxdfile>  
#
# Loop thorugh every layer of MXD:
#	Apply Symbology
#	Turn on Labels
#	Set Layer Scale and Transparency
#
# ---------------------------------------------------------------------------

# Import arcpy module
import arcpy

# Constants
#file_path = "S:\\GIS\\Projects\\WebMaps\\Interior\\AllFloors-QC\\"
file_path = "F:\\repos\\importFloorplan\\"
#labelExpression = "[ROOMEXT.RM_ID] + \"-\" + [ROOMEXT.Description]"
labelExpression = "[ROOMEXT.RM_ID]"


# Decode Parameters
mxd = arcpy.mapping.MapDocument(file_path + arcpy.GetParameterAsText(0))

# Define Dataframe
dataFrame = arcpy.mapping.ListDataFrames(mxd)[0]

# Loop through all layers starting with "Floor"
for lyr in arcpy.mapping.ListLayers(mxd, "Floor*", dataFrame):
        try:
            print ("\n" + lyr.name)

            # These layer transparencies do not come through to the service level
            print ("Set Layer Scale and Transparency")
            lyr.minScale = 2000
            lyr.transparency = 25
            lyr.visible = False

            # Apply Symbology
            print ("Applying Symbology")
            arcpy.ApplySymbologyFromLayer_management (lyr, file_path + "RoomSymbology.lyr")

            # NOT CURRENTLY USED, BUT COULD BE USEFUL
            # Add Unique Values
            # if lyr.symbologyType == "UNIQUE_VALUES":
            #   lyr.symbology.addAllValues()
        
            arcpy.RefreshActiveView()
            arcpy.RefreshTOC()

            # Turn on Labels
            print ("Turning on Labels")
            if lyr.supports("LABELCLASSES"):
                for lblClass in lyr.labelClasses:
                    lblClass.expression = labelExpression
                    lblClass.showClassLabels = True
            lyr.showLabels = True
        except Exception as err:
            print ("CANNOT PROCESS")

#save MXD
print("Saving MXD\n")
mxd.save()

#clean up pointer to mxd
del mxd
