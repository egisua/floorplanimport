# Set Minumim Scale for Label Visibily on All Layers of an MXD

# Arg 1: MXD file  (Rooms.mxd)
# Arg 2: min_scale (400.0)

# Uses comtypes and Arc Objects
# Includes a bunch of helper functions, perhaps not all necessary in this script.

import arcpy

def enumerate_feature_layers(map):
    uid = NewObj(esriSystem.UID, esriSystem.IUID)
    uid.value = "{E156D7E5-22AF-11D3-9F99-00C04F6BC78E}" # IGeoFeatureLayer
    enumLayer = map.Layers(uid, True)
    enumLayer.Reset()
    layer = enumLayer.Next()
    while layer:
        yield layer
        layer = enumLayer.Next()

def enumerate_label_classes(layer):
    labelClasses = CType(layer, esriCarto.IGeoFeatureLayer).AnnotationProperties
    for i in range(labelClasses.Count):
        labelClass, labelClassId = CType(labelClasses, esriCarto.IAnnotateLayerPropertiesCollection2).QueryItem(i)
        yield labelClass

def open_map_document(path):
    mxd = NewObj(esriCarto.MapDocument, esriCarto.IMapDocument)
    mxd.Open(path)
    return mxd

def GetESRIModule(module):
    """Returns the named ESRI module from comtypes.gen,
    creating the wrapper for it if necessary"""
    import sys
    mod = 'comtypes.gen.%s' % module
    try:
        __import__(mod, globals(), locals(), [module], -1)
    except ImportError:
        WrapModule(module)
        __import__(mod, globals(), locals(), [module], -1)
    return sys.modules[mod]

def GetLibPath():
    """Returns the location of the directory containing the ArcGIS object
    library (.olb) files"""
    import _winreg, os.path
    with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE,r"SOFTWARE\ESRI") as esrikey:
        # Lets first look for ArcGIS 10.0+ installations, e.g. HKLM\SOFTWARE\ESRI\Desktop10.0\InstallDir
        try:
            i = 0
            while True:
                subkeyname = _winreg.EnumKey(esrikey, i)
                if subkeyname.startswith("Desktop") or subkeyname.startswith("Engine"):
                    # Take the first one we find
                    with _winreg.OpenKey(esrikey, subkeyname) as subkey:
                        return os.path.join(_winreg.QueryValueEx(subkey, "InstallDir")[0], "com")
                i += 1
        except WindowsError:
            pass
        # Try the pre-10.0 key, e.g. HKLM\SOFTWARE\ESRI\ArcGIS\InstallDir
        try:
            with _winreg.OpenKey(esrikey, "ArcGIS") as subkey:
                return os.path.join(_winreg.QueryValueEx(subkey, "InstallDir")[0], "com")
        except WindowsError:
            pass
    raise WindowsError("ArcGIS InstallDir key not found!")

def WrapModules():
    """Force wrapping of all ArcObjects libraries (OLBs)"""
    import os.path
    import comtypes.client
    from glob import iglob
    comdir = GetLibPath()
    for olb in iglob(os.path.join(comdir, "*.olb")):
        comtypes.client.GetModule(olb)

def WrapModule(module):
    """Wrap a single ArcObjects library (OLB)"""
    import os
    import comtypes.client
    comdir = GetLibPath()
    olb = os.path.join(comdir, module + ".olb")
    comtypes.client.GetModule(olb)

def GetCurrentApp():
    """Returns the Application if the script is being run from
    within the application boundary of an ArcGIS application"""
    esriFramework = GetESRIModule("esriFramework")
    return NewObj(esriFramework.AppRef, esriFramework.IApplication)

def GetApp(app="ArcMap"):
    """Returns the Application if the script is being run from
    outside the application boundary of an ArcGIS application.
    "app" must be 'ArcMap' (default) or 'ArcCatalog'"""
    if not app in ("ArcMap", "ArcCatalog"):
        raise ValueError("app must be 'ArcMap' or 'ArcCatalog'")
    esriFramework = GetESRIModule("esriFramework")
    pAppROT = NewObj(esriFramework.AppROT, esriFramework.IAppROT)
    if pAppROT:
        pApp = None
        appcount = pAppROT.Count
        if appcount > 0:
            for i in range(appcount):
                pApp = pAppROT.Item(i)
                if pApp.Name == app:
                    break
        return pApp
    else:
        raise RuntimeError("AppROT object could not be created!")

def NewObj(MyClass, MyInterface):
    """Creates a new comtypes POINTER object where
    MyClass is the class to be instantiated,
    MyInterface is the interface to be assigned"""
    from comtypes.client import CreateObject
    try:
        ptr = CreateObject(MyClass, interface=MyInterface)
        return ptr
    except:
        return None

def CType(obj, interface):
    """Casts obj to interface and returns comtypes POINTER or None"""
    try:
        newobj = obj.QueryInterface(interface)
        return newobj
    except:
        return None

def CLSID(MyClass):
    """Return CLSID of MyClass as string"""
    return str(MyClass._reg_clsid_)

def print_label_classes(mxd_path):
    mxd = open_map_document(mxd_path)
    map = mxd.Map[0]
    layers = enumerate_feature_layers(map)
    for layer in layers:
        print layer.Name
        for labelClass in enumerate_label_classes(layer):
            print " name: {0}, minScale: {1}, maxScale: {2}".format(labelClass.Class, labelClass.AnnotationMinimumScale, labelClass.AnnotationMaximumScale)
    mxd.Close()

def set_label_scale(mxd_path, min_scale):
    mxd = open_map_document(mxd_path)
    map = mxd.Map[0]
    layers = enumerate_feature_layers(map)
    for layer in layers:
        #print layer.Name
        for labelClass in enumerate_label_classes(layer):
            # labelClass.AnnotationMinimumScale = 400.0
            labelClass.AnnotationMinimumScale = min_scale
            #print "\tname: {0}, minScale: {1}, maxScale: {2}".format(labelClass.Class, labelClass.AnnotationMinimumScale, labelClass.AnnotationMaximumScale)
    mxd.Save()


if __name__ == '__main__':
    esriSystem = GetESRIModule("esriSystem")
    esriCarto = GetESRIModule("esriCarto")
    #mxd_path = r"F:\repos\CADtoFCAutomation\Rooms.mxd"
    mxd_path = arcpy.GetParameterAsText(0)
    min_scale = float(arcpy.GetParameterAsText(1))
    set_label_scale(mxd_path, min_scale)
    print_label_classes(mxd_path)
    print "MXD Saved"