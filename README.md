# importFloorplan.py #

### Purpose ###
To convert AutoCad floorplans to GIS features and georeference them within a file GeoDatabase.  Features are symbolized, labeled and joined to additional data.

Usage: python importFloorplan.py [BuildingNumber]

Example: python importFloorplan.py 36

Building number is used to find CAD files in CAD_PATH. See CAD Directory for example files. (36-1.dwg, 36-2.dwg, etc.)

World files (*.wld) are also present in CAD_PATH and used to georeference the converted CAD.

### Requires ###
ESRI ArcMap and ArcPy installations

Python

### ARCHIBUS Integration ###
In order to connect additional data, this script leverages the connection established by ARCHIBUS, a join on Drawing Name + eHandle.

Without this, the script will still create features, but without room information (room numbers, building, etc)

### How do I get set up? ###

* Save a blank MXD file (Test.mxd)
* Save a blank File Geodatabase with ROOMEXT table added (see Test.GDB)
* Set File Paths

### Credits ###

* Drennen Brown (drennen@u.arizona.edu) - University of Arizona - Asst Director Planning Design and Construction Spatial Services
* Matt Rahr (rahr@email.arizona.edu) - University of Arizona - Director CALS Communications and Cyber Technologies
* Grant McCormick (grantmc@email.arizona.edu) - University of Arizona - GIS Manager Planning, Design and Construction