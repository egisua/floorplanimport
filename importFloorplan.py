# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# cadToFC.py
# Usage: cadToFC <Building_Number>
#
# 1) Convert one building's CAD files to Feature Classes within a file geodatabase using State Plane
# 2) World Files needed for georeferencing are in State Plane Coordinates
# 3) Reproject the feature class into Webmercator
# 4) Add layer to an MXD using new featureclass.  Refer to Level (B, 1, 2, etc) Crosswalk and append Level to layer name
# 5) Join to ROOMEXT (a stand alone table with Space data) using "Join_Key" = drawingName + eHandle
#
# Additional Scripts that can be run subsequently
# -----------------------------------------------
# layerProcessing.py - Sets symbology and labeling without changing/adding FCs or layers
# setMinLabelScale.py - Sets the minimum scale to show labels.
#
# REQUIRES
# --------
# Empty FGDB must be setup and ROOMEXT table added 
# Test.gdb provided as example
# MXD Can be blank/empty
#
#
# Notes
# -----
# We could eliminate what seems like a duplication of args Building Name / Number
# Except that multi-drawing files like 11+12.dwg, 11+12 makes an illegal layer name
# in this case we call "cadToFC 11and12 11+12"
#
#
# ---------------------------------------------------------------------------------

# Constants
FGDB = "myTest.gdb"
MXD = "Test.mxd"

# Import arcpy module
import arcpy

# Create Dictionary of Drawing / Actual Level Pairs
import csv
myfile = file('Level-Crosswalk.csv')
myfile.readline() # skip "date,value" line
levelCrosswalk = dict(csv.reader(myfile))

# Open logfile for append
fo = open("ConversionLog.txt", "a")

# Paths 
file_path = "F:\\repos\\importFloorplan\\"   # This is where GDB and MXD live
workspace = file_path + FGDB
cad_path = "S:\\Floorplans\\CAD\\"

mxdpath = file_path + MXD

# Pointer to MXD 
mxd = arcpy.mapping.MapDocument(mxdpath)

# Workspace
arcpy.env.workspace = workspace

#Tables
ROOMEXT = workspace + "\\ROOMEXT"

# Decode Parameters
BUILDINGNUM = arcpy.GetParameterAsText(0)
BUILDING = BUILDINGNUM.replace("+", "and") # Replace characters that would make invalid layer names

# Spatial Coordinates and Reprojections
COORD = "PROJCS['NAD_1983_StatePlane_Arizona_Central_FIPS_0202_Feet_Intl',GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',700000.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-111.9166666666667],PARAMETER['Scale_Factor',0.9999],PARAMETER['Latitude_Of_Origin',31.0],UNIT['Foot',0.3048]]"
ReprojectionFile = r"WGS 1984 Web Mercator (Auxiliary Sphere).prj"#Prepare a Reproject Object

validFloors = 0

print ("\nCREATE FEATURE CLASSES FOR BUILDING " + BUILDING)
print ("-------------------------------------------")
fo.write("\nCREATE FEATURE CLASSES FOR BUILDING " + BUILDING);

# Loop through all CAD Drawings for this building.  Break if we dont find another floor.
# Convert CAD into Feature Classes for our FGDB projected into WebMercator.

validFloors = 1
while 1==1: # This infinite loop set to break below

        FLOOR = str(validFloors)

        # Script arguments
        drawingName = BUILDINGNUM + "-" + FLOOR
        inputCADFile = drawingName + ".dwg"
        
        inputCADfc = cad_path + inputCADFile + "\\Polygon"

        if arcpy.Exists(inputCADfc):
            print ("FLOOR: " + FLOOR)

            try:
                levelcw = levelCrosswalk[drawingName]
            except Exception as err:
                print("\nCROSSWALK LOOKUP ERROR: Key not found")
                print("======BREAK LOOP=======")
                fo.write("\nCrosswalk Lookup Error: Key not found")
                break

            # Placeholder vars
            CADLayer = "CADLayer_" + FLOOR
            outFeatureClassName = "Floor" + BUILDING + "_" + FLOOR + "_" + levelcw + "_SP" 
            outFeatureClassNameWM = "Floor" + BUILDING + "_" + FLOOR + "_" + levelcw 

            # Count how many valid floors we add
            validFloors = validFloors + 1

            # Process: Make CAD Layer
            print "Create CAD Layer" #filter by RM$ or RAPL and hide some fields
            arcpy.MakeFeatureLayer_management(inputCADfc, CADLayer, "Layer = 'RM$' OR Layer = 'RAPL'", "", "FID FID HIDDEN NONE;Shape Shape HIDDEN NONE;Entity Entity HIDDEN NONE;Handle Handle VISIBLE NONE;Layer Layer HIDDEN NONE;LyrFrzn LyrFrzn HIDDEN NONE;LyrLock LyrLock HIDDEN NONE;LyrOn LyrOn HIDDEN NONE;LyrVPFrzn LyrVPFrzn HIDDEN NONE;LyrHandle LyrHandle HIDDEN NONE;Color Color HIDDEN NONE;EntColor EntColor HIDDEN NONE;LyrColor LyrColor HIDDEN NONE;BlkColor BlkColor HIDDEN NONE;Linetype Linetype HIDDEN NONE;EntLinetype EntLinetype HIDDEN NONE;LyrLnType LyrLnType HIDDEN NONE;BlkLinetype BlkLinetype HIDDEN NONE;Elevation Elevation HIDDEN NONE;Thickness Thickness HIDDEN NONE;LineWt LineWt HIDDEN NONE;EntLineWt EntLineWt HIDDEN NONE;LyrLineWt LyrLineWt HIDDEN NONE;BlkLineWt BlkLineWt HIDDEN NONE;RefName RefName HIDDEN NONE;LTScale LTScale HIDDEN NONE;ExtX ExtX HIDDEN NONE;ExtY ExtY HIDDEN NONE;ExtZ ExtZ HIDDEN NONE;DocName DocName VISIBLE NONE;DocPath DocPath HIDDEN NONE;DocType DocType HIDDEN NONE;DocVer DocVer HIDDEN NONE")
            #arcpy.MakeFeatureLayer_management(inputCADfc, CADLayer, "Layer = 'RM$'")

            #Convert CAD to Feature Class
            print "Convert CAD to Feature Class"

            #CLEANUP, let's remove an existing feature class SP
            if arcpy.Exists(workspace + "\\" + outFeatureClassName): 
                    print("Delete SP " + outFeatureClassName)
                    arcpy.Delete_management(workspace  + "\\" + outFeatureClassName, "")

            #CLEANUP, let's remove an existing feature class WM
            if arcpy.Exists(workspace + "\\" + outFeatureClassNameWM): 
                    print("Delete WM " + outFeatureClassNameWM)
                    arcpy.Delete_management(workspace  + "\\" + outFeatureClassNameWM, "")

            #Creating a ESRI feature class based on the CADLayer
            arcpy.FeatureClassToFeatureClass_conversion(CADLayer, workspace, outFeatureClassName, "", "Handle \"Handle\" true true false 16 Text 0 0 ,First,#,CADLayer,Handle,-1,-1;DocName \"DocName\" true true false 255 Text 0 0 ,First,#,CADLayer,DocName,-1,-1", "")

            #Define State Plan Projection
            print "Define State Plane Projection"
            arcpy.DefineProjection_management(in_dataset=workspace + "\\" + outFeatureClassName, coor_system=COORD)

            #Reproject to Web Mercator
            print "ReProject into Web Mercator\n"
            outputCS = arcpy.SpatialReference(ReprojectionFile)
            transformation = "NAD_1983_To_WGS_1984_1"            
            arcpy.Project_management(outFeatureClassName, outFeatureClassNameWM, outputCS, transformation)

            #Remove State Plan Feature Class
            arcpy.Delete_management(workspace  + "\\" + outFeatureClassName, "")

            #END OF LOOP - finished populating the GDB with converted cad layers into feature classes
        else:
            break


# Remove Existing Layers
print ("\nREMOVE EXISTING LAYERS")
print ("-------------------")
dataFrame = arcpy.mapping.ListDataFrames(mxd)[0]
for lyr in arcpy.mapping.ListLayers(mxd, "Floor" + BUILDING + "_*", dataFrame):
        arcpy.mapping.RemoveLayer(dataFrame, lyr)
        print(lyr.name)



# Create feature layers and add them to the MXD
print ("\n\nCREATE FEATURELAYERS AND ADD TO MAP")
print ("-------------------")
for i in range(1, int(validFloors)):
        FLOOR = str(i)
        drawingName = BUILDINGNUM + "-" + FLOOR
        try:
            fc = "Floor" + BUILDING + "_" + FLOOR + "_" + levelCrosswalk[drawingName]
        except Exception as err:
            print("\nCROSSWALK LOOKUP ERROR: Key not found.  Drawings out of sync with Crosswalk?")
            fo.write("\nCrosswalk Lookup Error: Key not found.  Drawings out of sync with Crosswalk?")        

        # Create layer based on fc
        lyrName = fc
        print (lyrName)
        layer = arcpy.MakeFeatureLayer_management(workspace + "\\" + fc, lyrName, "", "", "Shape Shape VISIBLE NONE;Handle Handle VISIBLE NONE;DocName DocName VISIBLE NONE")
        
        # Add Layer to Map
        addlayer = arcpy.mapping.Layer(lyrName)
        lyr = arcpy.mapping.AddLayer(dataFrame, addlayer, "BOTTOM")


# Modify Newly Added Layers - Add Symbology, Set Scale / Visibility, Add Fields and Apply Join
print ("\n\nMODIFY LAYERS")
print ("--------------")
for lyr in arcpy.mapping.ListLayers(mxd, "Floor" + BUILDING + "_*", dataFrame):
        print ("LAYER: " + lyr.name)

        # Add Fields we need for permenant key
        print ("Add key field")
        arcpy.AddField_management(lyr, "Join_Key", "TEXT", 50)
        arcpy.CalculateField_management(lyr, "Join_Key", "[DocName]+\"+\"+[Handle]")

        # Perform Join
        print ("Join ROOMEXT Table\n")
        arcpy.AddJoin_management(lyr, "Join_Key", ROOMEXT, "Join_Key", "KEEP_ALL")

        # These layer transparencies do not come through to the service level
        print ("Set Layer Scale and Transparency")
        lyr.minScale = 2000
        lyr.transparency = 25
        lyr.visible = False

        #apply symbology
        #Maybe this could be used to set all the other aspects of the layer I'd want?  Fit to Polygon, etc.
        print ("Apply Symbology")
        arcpy.ApplySymbologyFromLayer_management (lyr, file_path + "RoomSymbology.lyr")

        #turn on labels
        print ("Turn on Room Number Labels")
        if lyr.supports("LABELCLASSES"):
            for lblClass in lyr.labelClasses:
                lblClass.expression = "[ROOMEXT.RM_ID]"
                lblClass.showClassLabels = True
        lyr.showLabels = True



# Save MXD
print("SAVING MXD - " + mxdpath)
print("COMPLETE")
fo.write("\nMXD Saved - " + mxdpath)
fo.close()
mxd.save()

# Cleanup pointer
del mxd
